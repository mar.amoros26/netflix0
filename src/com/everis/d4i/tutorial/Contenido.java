package com.everis.d4i.tutorial;

public class Contenido
{		
	
		//De cada contenido, he de guardar el nombre, la fecha de reproducción, la valoración (si la hay), y el ID del contenido.
		private String nombre;
		private String fecha;
		private String valoracion ="Sin valorar";
		private int id;
		
	//Constructor
		public Contenido(String nombre, String fecha, String valoracion, int id) 
		{
			super();
			this.nombre = nombre;
			this.fecha = fecha;
			this.valoracion = valoracion;
			this.id = id;
		}
		
		public Contenido(String nombre) 
		{
			super();
			this.nombre = nombre;
		
		}
		public Contenido() 
		{
			
		}
	
	//setters y getters
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		public String getValoracion() {
			return valoracion;
		}
		public void setValoracion(String valoracion) {
			this.valoracion = valoracion;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	
	
//to string
		@Override
		public String toString() {
			return "nombre=" + nombre + ", fecha=" + fecha + ", valoracion=" + valoracion + ", id=" + id ;
		}
	
	
	
}
