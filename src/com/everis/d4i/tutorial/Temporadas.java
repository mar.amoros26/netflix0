package com.everis.d4i.tutorial;

public class Temporadas extends Serie {

	private int numeroTemporada;
	private String valoracionTemp="Sin valorar";
	
	


	public int getNumeroTemporada() {
		return numeroTemporada;
	}






	public void setNumeroTemporada(int numeroTemporada) {
		this.numeroTemporada = numeroTemporada;
	}






	public String getValoracionTemp() {
		return valoracionTemp;
	}






	public void setValoracionTemp(String valoracionTemp) {
		this.valoracionTemp = valoracionTemp;
	}






	public Temporadas(String nombre, String fecha, String valoracion, int id, String studio,
			int firstSeasonRelease) {
		super(nombre, fecha, valoracion, id, studio, firstSeasonRelease);
	}






	public Temporadas(int numeroTemporada, String valoracionTemp) {
		
		this.numeroTemporada = numeroTemporada;
		this.valoracionTemp = valoracionTemp;
	}
	
	public Temporadas() {
			
		
		}










	//to String
	public String toString() {
		return "numeroTemporada=" + numeroTemporada + ", valoracion=" + valoracionTemp ;
	}
	
	
	
}
