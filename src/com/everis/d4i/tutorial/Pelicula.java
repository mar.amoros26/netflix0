package com.everis.d4i.tutorial;

public class Pelicula extends Contenido {
	
	//Pel�culas: el nombre del director, y el a�o de publicaci�n
		private int publicacion;
		private String director;

	
	
	//Constructor
		public Pelicula(String nombre, String fecha, String valoracion, int id,String director,int publicacion) {
			super(nombre, fecha, valoracion, id);
			this.director=director;
			this.publicacion=publicacion;
			
		}
		
		public Pelicula(String nombre,String director) {
			super(nombre);
			this.director=director;
			
		}
		public Pelicula() {
		
			
		}


		//setters y getters
			public String getdirector() {
				return director;
			}
	
	
	
			public void setdirector(String director) {
				this.director = director;
			}
	
	
	
			

			public int getpublicacion() {
				return publicacion;
			}
	
	
	
			public void setpublicacion(int publicacion) {
				this.publicacion = publicacion;
			}
		
	//toString
			@Override
			public String toString() {
				
				return  super.toString()+" " + "publicacion=" + publicacion + ", director=" + director ;
			}

		

}
