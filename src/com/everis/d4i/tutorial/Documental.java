package com.everis.d4i.tutorial;

public class Documental extends Contenido {
	
	//Documentales: el director y el a�o de publicaci�n.
		private String director;
		private int publicacion;

	
	
	//Constructor
		public Documental(String nombre, String fecha, String valoracion, int id,String director,int publicacion) {
			super(nombre, fecha, valoracion, id);
			this.director=director;
			this.publicacion=publicacion;
			
		}
		
		public Documental(String nombre) {
			super(nombre);
		
			
		}
		public Documental() {
			
			
		}


		//setters y getters
			public String getdirector() {
				return director;
			}
	
	
	
			public void setdirector(String director) {
				this.director = director;
			}
	
	
	
			public int getpublicacion() {
				return publicacion;
			}
	
	
	
			public void setpublicacion(int publicacion) {
				this.publicacion = publicacion;
			}
		
			//toString
			@Override
			public String toString() {
				
				return  super.toString()+" " + "publicacion=" + publicacion + ", director=" + director ;
			}
	
		

}
