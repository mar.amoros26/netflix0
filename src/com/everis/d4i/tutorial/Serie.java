package com.everis.d4i.tutorial;

import java.util.ArrayList;

public class Serie extends Contenido 
{
	//Series: el nombre del studio, el n�mero de temporadas y el a�o de la primera temporada
		private String studio;
		private int firstSeasonRelease;
		private ArrayList<Temporadas> seasson;
		private int seassonNum;
		
		
	public int getSeassonNum() {
			return seassonNum;
		}

		
		public void setSeassonNum(int seassonNum) {
			this.seassonNum = seassonNum;
		}

		//Constructor
		public Serie(String nombre, String fecha, String valoracion, int id, String studio,
				int firstSeasonRelease) 
		{
			super(nombre, fecha, valoracion, id);
			this.studio = studio;
			this.firstSeasonRelease = firstSeasonRelease;
		}
		
		public Serie(String nombre) 
		{
			super(nombre);
			
		}
		
		public Serie() 
		{
			
		}

	//Setters y getters
		public String getstudio() {
			return studio;
		}

		public void setstudio(String studio) {
			this.studio = studio;
		}
		
		public ArrayList<Temporadas> getSeasson() {
			return seasson;
		}

		public void setSeasson(ArrayList<Temporadas> seasson) {
			this.seasson = seasson;
		}

		

		public int getfirstSeasonRelease() {
			return firstSeasonRelease;
		}

		public void setfirstSeasonRelease(int firstSeasonRelease) {
			this.firstSeasonRelease = firstSeasonRelease;
		}
	
	//toString
		/*	 studio;
		 numTemporadas;
		 firstSeasonRelease;*/
		@Override
		public String toString() {
			
			return  super.toString()+" " + "studio=" + studio + ", firstSeasonRelease=" + firstSeasonRelease 
					+"Temporadas="+ seasson;
		}
		
}
