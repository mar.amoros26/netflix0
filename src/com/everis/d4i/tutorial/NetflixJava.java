package com.everis.d4i.tutorial;

import java.util.ArrayList;

public class NetflixJava {

//aaaaaaaaaa
	
    public static void main(String[] args) {
    	
    ArrayList<Contenido> watched = new ArrayList<>();

	/*1
	 *Recientemente he visto la peli 
	 *The clockwork orange de Cubrick, el documental 
	 *the Blue Planet y Stranger Things. No las he valorado por
	 * el momento. (incialización) */
 
    	System.out.println("1");
    	watched.add(new Pelicula("The clockwork orange","Cubrick"));
    	watched.add(new Documental("the Blue Planet"));
    	watched.add(new Serie("Stranger Things"));
    	
    	printWatched(watched);
    	
    	/*2
    	 * 2.Revisionando Stranger things, decido que la serie me gusta, 
    	 * pero la segunda temporada no. Lo vuelvo a registrar
    	 */
    	System.out.println("2");
    	for (Contenido contenido : watched) { 
    		
			if (contenido.getNombre().equalsIgnoreCase("Stranger Things")) {
				ArrayList<Temporadas> seassons=  new ArrayList<>();
				Serie serie=(Serie)contenido;
				
				Temporadas t1=new Temporadas(1, null);
				Temporadas t2=new Temporadas(2, "5"); //La valoracion es 1-5 estrellas o "Sin valorar"
				
		
				seassons.add(t1);
				seassons.add(t2);
				
			}
    		
		}
    	
    	printWatched(watched);
    	
    	
    	/*3
    	 * 3.oops, la naranja mecanica es de Kubrick. Lo corrijo.
    	 */
    	System.out.println("3");
    	for (Contenido contenido : watched) {
    		
			if (contenido.getNombre().equalsIgnoreCase("The clockwork orange")) {
				Pelicula peli=(Pelicula)contenido;
				peli.setdirector("Kubrick");
				
			
			}
    		
		}
    	
    	printWatched(watched);
    	
    	/*4
    	 * 4.Mi hermana me roba la cuenta de Netflix y mira the Notebook (Y lo registra)
    	 */
    	System.out.println("4");
    	Pelicula peli= new Pelicula();
    	peli.setNombre("the Notebook");
    	watched.add(peli);
    	
    	printWatched(watched);
    	
    	
    	
    	/*5
    	 * Revisando el registro, me doy cuenta de ello 
    	 * y lo borro inmediatamente. Doy valoraciones a
    	 *  las peliculas que no tienen.
    	 */
    	System.out.println("5");
    	
    	watched.get(0).setValoracion("2"); //solo la peli
    	
    	
    	
    	Contenido toRemove = null;
    	for (Contenido contenido : watched) {
			if (contenido.getNombre().equals("the Notebook")) {
				toRemove=contenido;
			}
		}
    	
    	watched.remove(toRemove);
    	printWatched(watched);
    	
    	
    	/*6
    	 * Ayer hice maraton de Stephen King y vi the Shining 
    	 * y Pet Sematary. Ambas me gustaron mucho, aunque 
    	 * la primera mas que la segunda.
    	 */
    	System.out.println("6");
    	Pelicula peli2= new Pelicula();
    	peli2.setNombre("the Shining");
    	peli2.setValoracion("5");
    	watched.add(peli2);
    	
    	Pelicula peli3= new Pelicula();
    	peli3.setNombre("Pet Sematary");
    	peli3.setValoracion("4");
    	watched.add(peli3);
    	
    	printWatched(watched);
    	
    	
    	/*7
    	 * Filtro las peliculas por valoracion: solo busco las que me han gustado mas.
    	 */
    	System.out.println("7");
    	for (Contenido contenido : watched) { 
			if (contenido.getValoracion().equals("4") || contenido.getValoracion().equals("5")) {
				System.out.println(contenido);
				
			}
		}
    }
    
    
    
    
    
    
    

	private static void printWatched(ArrayList<Contenido> watched) {
		for (Contenido contenido : watched) {
			System.out.println(contenido);
		}
	}
    
    
    
    
    
    
    
 }